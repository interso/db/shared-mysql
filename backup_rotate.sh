#!/bin/bash

source .env
source .env.local

if [ "$BACKUP_DATA_DIR" == "" ]; then
   echo "Not configure data dir for store backup. Set env BACKUP_DATA_DIR in .env.local"
   exit 1
fi;

if [ ! -d "$BACKUP_DATA_DIR" ]; then
   echo "$BACKUP_DATA_DIR is not correct directory"
   exit 1
fi;

FOLDERS=$(ls -1 $BACKUP_DATA_DIR | sort -n -r)

# Счетчик для отслеживания количества оставленных папок
COUNT=0

# Проходим по каждой папке
while read -r FOLDER; do
    ((COUNT++))
    
    # Если это пятая или более старая папка, удаляем ее
    if [ $COUNT -gt $BACKUP_DAYS ]; then
        BACKUP_DAY_DIR="$BACKUP_DATA_DIR/$FOLDER"
        echo "Удаление папки: $BACKUP_DAY_DIR"
        rm -rf "$BACKUP_DAY_DIR"
    fi
done <<< "$FOLDERS"
