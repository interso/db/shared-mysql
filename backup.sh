#!/bin/bash

source .env
source .env.local

if [ "$BACKUP_DATA_DIR" == "" ]; then
   echo "Not configure data dir for store backup. Set env BACKUP_DATA_DIR in .env.local"
   exit 1
fi;

if [ ! -d "$BACKUP_DATA_DIR" ]; then
   echo "$BACKUP_DATA_DIR is not correct directory"
   exit 1
fi;

DATE=`date +%Y-%m-%d`
DAY_BACKUP="$BACKUP_DATA_DIR/$DATE"

mkdir -p $DAY_BACKUP

ALL_DATABASES=`make db-list | grep -v _schema | grep -v mysql | grep -v sys`

for DATABASE in $ALL_DATABASES; do
   echo -e "\nBackup $DATABASE ($DATE)"
   DB_ONE_DAY_BACKUP="$DAY_BACKUP/${DATABASE}.sql.gz" 
   if [ -r "$DB_ONE_DAY_BACKUP" ]; then
      echo "[W] Skip dump database $DATABASE. Backup file already created $DB_ONE_DAY_BACKUP"
   else
      touch $DB_ONE_DAY_BACKUP 
      make db-export d=$DATABASE | gzip > $DB_ONE_DAY_BACKUP 
   fi;

done;
