SHELL = /bin/bash
BASE_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
ROOT_DIR := $(BASE_DIR)/.make

SERVICE_NAME := shared_db

include $(ROOT_DIR)/init.mk

.PHONY: install pull pull-images network clean
### build ###
install: pull network up wait-db-connect ps ##@build First run.

clean: confirm ##@build Stop all and removes containers, networks, volumes, and images
	@$(DC) down -v --remove-orphans
	@$(DC) rm -f -v

pull-images: pull
pull: ##@build Pull images
	@$(DC) pull

network: ##@build Create default network
	@echo "Create network $(EXTERNAL_NETWORK)"
	@docker network inspect $(EXTERNAL_NETWORK) 1>/dev/null 2>/dev/null || docker network create $(EXTERNAL_NETWORK)
	@echo "Complete network creation"

### build ###

.PHONY: up start stop restart
### process ###
up: ##@process Up container in background
	@$(DC) up -d

up-recreate: ##@process Up container and recreate container
	@$(DC) up -d --force-recreate

start: ##@process Start container
	@$(DC) start 

stop: ##@process Stop container
	@$(DC) stop 

restart: stop start ##@process Restart container
### process ###

.PHONY: bash bash-db
### shell ###
bash: bash-db ##@shell Alias bash-db

bash-db: ##@shell Exec bash on database container
	@$(DC_EXEC)  --env LANG=C.UTF-8 $(SERVICE_NAME) bash

### shell ###

.PHONY: ps logs
### info ###
ps: ##@info Show status
#	@$(DC) ps
	@$(DC) ps --format "table {{.Service}}\t{{.Status}}\t{{.RunningFor}}\t{{.Image}}\t{{.Ports}}" $(c)

logs: ##@info Show all logs
	@$(DC) logs --tail=300 -f
### info ###

.PHONY: db db-list db-ping wait-db-check
### database ###
db: ##@database Run database console (c=<database name>)
	$(DC_EXEC) --env LANG=C.UTF-8  $(SERVICE_NAME) mysql -u root -p$(MYSQL_ROOT_PASSWORD) $(c)

db-ping: ##@database Db ping
	@$(DC_EXEC) --env LANG=C.UTF-8 $(SERVICE_NAME) /bin/bash -c "mysqladmin -p$(MYSQL_ROOT_PASSWORD) ping 2>/dev/null"

wait-db-connect: ##@database check db connect and wait
	@n=0; \
	result=""; \
	echo "Wait database is online (first time is 35 sec or more)"; \
	while [[ $${n} -lt 60 ]] ; do \
		n=`expr $$n + 1`; \
		result=`$(DC_EXEC) --env LANG=C.UTF-8 $(SERVICE_NAME) bash -c "mysqladmin -p$(MYSQL_ROOT_PASSWORD) ping 2>/dev/null"`; \
		result=`echo -e "$$result" | sed -r 's/\s+//g'`; \
		if [[ "$$result" = "mysqldisalive" ]]; then echo "Database is online"; break; fi; \
		echo -n "*"; \
		sleep 5; \
	done; \
	true

db-list: ##@db-cmd Show databases list
	@echo 'show databases' | $(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) mysql -B -N -u root -p$(MYSQL_ROOT_PASSWORD) 2>/dev/null

db-create: ##@db-cmd Create new database c=<database name>
	@echo "Create database $(c)"
	@$(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) bash -c "mysqladmin -p$(MYSQL_ROOT_PASSWORD) create $(c) 2>/dev/null"
	@echo "Creating complete"
 
db-drop: ##@db-cmd Drop database c=<database name>
	@echo "Drop database $(c)"
	@$(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) bash -c "mysqladmin -p$(MYSQL_ROOT_PASSWORD) drop $(c) 2>/dev/null"
	@echo "Droping complete"

user-list: ##@db-user-cmd List users
	@echo "SELECT user, host FROM user;" | $(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) mysql -u root -p$(MYSQL_ROOT_PASSWORD) mysql 2>/dev/null

user-create: ##@db-user-cmd Create new user u=<username> p=<password>
	@echo "Create user $(u)"
	@echo "CREATE USER '$(u)'@'%' IDENTIFIED WITH caching_sha2_password BY '$(p)';" | $(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) mysql -u root -p$(MYSQL_ROOT_PASSWORD) mysql 2>/dev/null

user-update: ##@db-user-cmd Alter exist user u=<username> p=<password>
	@echo "Alter user $(u)"
	@echo "ALTER USER '$(u)'@'%' IDENTIFIED WITH caching_sha2_password BY '$(p)';" | $(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) mysql -u root -p$(MYSQL_ROOT_PASSWORD) mysql 2>/dev/null

user-drop: ##@db-user-cmd Drop user u=<username>
	@echo "Drop user $(u)"
	@echo "DROP USER '$(u)'@'%';" | $(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) mysql -u root -p$(MYSQL_ROOT_PASSWORD) mysql 2>/dev/null

user-grant: ##@db-user-cmd Add user u=<username> all privileges to db d=<database>
	@echo "Grant user $(u) to database $(d)"
	@echo "GRANT ALL PRIVILEGES ON $(d).* TO '$(u)'@'%' WITH GRANT OPTION; FLUSH PRIVILEGES;" | $(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) mysql -u root -p$(MYSQL_ROOT_PASSWORD) mysql 2>/dev/null



db-export: ##@db-exchange Export db from database d=<database name>. Use pipe
	@$(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) mysqldump --opt -u root -p$(MYSQL_ROOT_PASSWORD) $(d) 2>/dev/null

db-import: ##@db-exchange Import from c=<filename> to database d=<dbname>
	@cat $(c) | $(DC_EXEC) -T --env LANG=C.UTF-8 $(SERVICE_NAME) mysql -u root -p$(MYSQL_ROOT_PASSWORD) $(d)


backup: ##@backup Backup all databases
	@$(BASE_DIR)/backup.sh	

### database ###

